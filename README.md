# Ionic Login with firebase

### Create a new project 
* ```ionic start myApp blank```   
(blank|tabs|sidemenu) = templates
* Choose Angular in terminal

### Start server preview
```ionic serve```   

### Generate a new page
```ionic g page login``` 

### Generate a module file
```ionic g module components```  
*It will generate a dir components within a components.module.ts file*    
### Generate a new component in components dir
```ionic g component components/mycomponent```  

### Generate a new guard in guards dir   
```ionic g guard guards/auth --implements CanLoad```  
## Firebase 
[https://console.firebase.google.com/project](https://console.firebase.google.com/project)
* Create a new project    
* Get the generate code ```{  apiKey: ‘AI….appId: ‘1:7}```   
* Continue to console > menu Authentication > Sign-In method    
* Ajouter la méthode (Adresse mail/password)   

#### On app side
* Install firebase dependency    
```npm i firebase @angular/fire```         

#### /environnement.ts file > const environment = {} add property    
  ```firebase:{ the generated code from firebase '{  apiKey: ‘AI….appId: ‘1:7}'}```   

#### /app.module.ts >
  * Import the Firebase modules   
```#javascript
import { AngularFireModule } from '@angular/fire/compat';
import { AngularFireAuthModule } from '@angular/fire/compat/auth';
// import { AngularFireStorageModule } from '@angular/fire/compat/storage';
// import { AngularFirestoreModule } from '@angular/fire/compat/firestore';
// import { AngularFireDatabaseModule } from '@angular/fire/compat/database';
```   

* Import your environments variables module   
```import { environment } from '../environments/environment’;```    

* Initialize firebase with our environement by adding the module in @NgModule({ > imports[     
```AngularFireModule.initializeApp(environment.firebase)```   
```AngularFireAuthModule```    


#### component.page.ts
* ```import { AngularFireAuth } from '@angular/fire/compat/auth';```   
* add in constructor > ```ngFireAuth: AngularFireAuth```   
* Create a user object ‘user = {email:’’,password:’’}’

## Toast 
[https://ionicframework.com/docs/api/toast](https://ionicframework.com/docs/api/toast)

* Import in our component.page.ts
* ```import { ToastController } from '@ionic/angular';```    
* Add in constructor ```public toastController: ToastController```  

## Generate first component
* Generate new module file for components dir
```ionic g module components```  
* Generate a new component in components dir
```ionic g component components/mycomponent```  
  ### In components module 
  * create the const PAGES 
  * Add it to imports and exports 
```#javascript
const PAGES = [
  DeconectionBtnComponent
];

@NgModule({
  declarations: PAGES,
  exports: PAGES,
  imports: [
    CommonModule
  ]
})
```
### In appp.module.ts
In order to imports all components   
* Add ```import { ComponentsModule } from './components/components.module';```  
* Add to ```ComponentsModule``` in imports array ngModule   
  
### Add it the desired page to show module
* in mypage.module.ts > ```import { ComponentsModule } .....dule';``` & in imports array ```ComponentsModule```    
* in mypage.page.html > ```<app-mycomponent-name></app-mycomponent-name>```   
 
 ### Ionic STorage Angular
 [https://github.com/ionic-team/ionic-storage](https://github.com/ionic-team/ionic-storage)   
```npm install @ionic/storage-angular```  

#### Add in app-module.ts   
* ```import { IonicStorageModule } from '@ionic/storage-angular';```   
* ```IonicStorageModule.forRoot()```   


#### Add in a component 
* ```import { Storage } from '@ionic/storage-angular';```  
* ```constructor(private storage: Storage) {}```   
* ```await this.storage.create();```  

#### Set, get, remove
* ```this.storage.set('key', 'value');```  
* ```this.storage.get('key');```  
* ```this.storage.remove('key');```    
