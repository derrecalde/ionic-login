import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { ToastController } from '@ionic/angular';
import { StorageService } from '../../services/storage.service';

@Component({
  selector: 'app-deconection-btn',
  templateUrl: './deconection-btn.component.html',
  styleUrls: ['./deconection-btn.component.scss'],
})
export class DeconectionBtnComponent implements OnInit {

  constructor(private router: Router,
    public ngFireAuth: AngularFireAuth, public toastController: ToastController,
    private storageService: StorageService) { }

    ngOnInit() {}

  async toast(message: string, color: string = 'primary') {
    const toast = await this.toastController.create({
      message,
      duration: 2000,
      color // "primary", "secondary", "tertiary", "success", "warning", "danger", "light", "medium", and "dark"
    });
    toast.present();
  }

  async logOut(){
    await this.ngFireAuth.signOut()
    .then( (result) =>{
      this.storageService.remove('userEmail');
      this.router.navigate(['/login']);
      this.toast('Deconected !','success');
    }).catch((error)=>{
      this.toast(error.message,'danger');
    });
  }

}
