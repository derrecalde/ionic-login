import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DeconectionBtnComponent } from './deconection-btn/deconection-btn.component';
import { MenuComponent } from './menu/menu.component';

const PAGES = [
  DeconectionBtnComponent,
  MenuComponent
];

@NgModule({
  declarations: PAGES,
  exports: PAGES,
  imports: [
    CommonModule
  ]
})
export class ComponentsModule { }
