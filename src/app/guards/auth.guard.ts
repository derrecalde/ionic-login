import { Injectable } from '@angular/core';
import { CanLoad, Route, UrlSegment, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { StorageService } from '../services/storage.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanLoad {

  constructor(private router: Router, private storageService: StorageService){}

  async canLoad(){
    const userEmail = await this.storageService.get('userEmail');

    const isAuthenticated = userEmail ? 1: 0;
    if (isAuthenticated){
      return true;
    }else{
      this.router.navigateByUrl('/');
      return false;
    }
  }
}
