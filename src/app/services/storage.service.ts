import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage-angular';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  private $storage: Storage | null = null;

  constructor(private storage: Storage) {}

  async initStorage() {
    const storage = await this.storage.create();
    this.$storage = storage;
  }

  async set(key: string, value: string|object){
    await this.initStorage();
    this.$storage.set(key, value);
  }

  async get(key: string){
    await this.initStorage();
    return this.$storage.get(key);
  }

  async remove(key: string){
    await this.initStorage();
    this.$storage.remove(key);
  }

}
