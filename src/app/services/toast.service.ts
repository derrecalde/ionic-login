import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  constructor(private toastController: ToastController) { }

  async toast(message: string, color: string = 'primary') {
    const toast = await this.toastController.create({
      message,
      duration: 2000,
      color // "primary", "secondary", "tertiary", "success", "warning", "danger", "light", "medium", and "dark"
    });
    toast.present();
  }

}
