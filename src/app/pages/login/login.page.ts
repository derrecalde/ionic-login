import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { ToastService } from '../../services/toast.service';
import { StorageService } from 'src/app/services/storage.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  //Init User object
  user = {
    email: '',
    password: ''
  };

  constructor(private router: Router,
    public ngFireAuth: AngularFireAuth ,
    public toastService: ToastService,
    private storageService: StorageService,
    private navController: NavController) { }

  ngOnInit() {}

  async logIn(){
    if (this.user.email && this.user.password){
      const user = await this.ngFireAuth.signInWithEmailAndPassword(this.user.email, this.user.password)
      .then( (result) =>{
        // this.router.navigate(['/home']);
        this.navController.navigateRoot('home');
        this.toastService.toast('Connected !','success');
        this.storageService.set('userEmail', result.user.email);
      }).catch((error)=>{
        this.toastService.toast(error.message,'danger');
      });
    }else{
      this.toastService.toast('Empty fields !','warning');
    }
  }

  async register(){
    if (this.user.email && this.user.password){
      await this.ngFireAuth.createUserWithEmailAndPassword(this.user.email, this.user.password)
      .then( (result) =>{
        this.storageService.set('userEmail', result.user.email);
        this.toastService.toast('Registration success !','success');
      }).catch((error)=>{
        this.toastService.toast(error.message,'danger');
      });
    }else{
      this.toastService.toast('Empty fields !','warning');
    }
  }
}
