import { Component, OnInit } from '@angular/core';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  user = {
    email: ''
  };

  constructor(private storageService: StorageService) { }

 async ngOnInit() {
  this.user.email = await this.storageService.get('userEmail');
  }

}
