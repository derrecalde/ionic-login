import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SubmitionPageRoutingModule } from './submition-routing.module';

import { SubmitionPage } from './submition.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SubmitionPageRoutingModule
  ],
  declarations: [SubmitionPage]
})
export class SubmitionPageModule {}
