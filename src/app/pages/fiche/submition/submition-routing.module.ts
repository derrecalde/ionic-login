import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SubmitionPage } from './submition.page';

const routes: Routes = [
  {
    path: '',
    component: SubmitionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SubmitionPageRoutingModule {}
