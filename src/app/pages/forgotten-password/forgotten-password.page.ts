import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { ToastService } from '../../services/toast.service';

@Component({
  selector: 'app-forgotten-password',
  templateUrl: './forgotten-password.page.html',
  styleUrls: ['./forgotten-password.page.scss'],
})
export class ForgottenPasswordPage implements OnInit {

  //Init User object
  user = {
    email: ''
  };

  constructor(private router: Router,
    public ngFireAuth: AngularFireAuth ,
    public toastService: ToastService) { }

  ngOnInit() {
  }

  async resetPassword(){
    await this.ngFireAuth.sendPasswordResetEmail(this.user.email)
    .then((result) => {
      this.toastService.toast('You should have received an email','success');
    }).catch((error)=>{
      this.toastService.toast(error.message,'danger');
    });
  }

}
